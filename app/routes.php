<?php
$router->get('', 'PagesController@home');
$router->get('tasks', 'TaskController@index');
$router->post('tasks/store', 'TaskController@store');