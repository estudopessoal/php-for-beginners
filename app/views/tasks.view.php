<?php require 'partials/head.php' ?>

<h1>Add A Task</h1>

<form action="/tasks/store" method="POST">

  <input type="text" name="description" placeholder="Write your task">
  <button>Send</button>

</form>

<h1>All Tasks</h1>
<ul>
  <?php foreach($tasks as $task): ?>
  
  <li>
    <?php if($task->getCompleted()): ?>
    
    <strike><?= $task->getDescription() ?></strike>
    
    <?php else: ?>
    
    <?= $task->getDescription() ?>
    
    <?php endif ?>
  </li>
  
  <?php endforeach ?>
</ul>

<?php require 'partials/footer.php' ?>