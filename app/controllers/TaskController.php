<?php

namespace App\Controllers;

use App\Core\App;
use App\Models\Task;

class TaskController
{
  public function index()
  {
    return view('tasks', [ 'tasks' => Task::all() ]);
  }

  public function store()
  {
    Task::add([
      'description' => $_POST['description'],
      'completed'   => 0
    ]);
    
    return redirect('tasks');
  }

}
