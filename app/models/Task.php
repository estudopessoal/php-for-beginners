<?php

namespace App\Models;

use App\Core\App;

class Task 
{

  protected $description;
  protected $completed = false;

  public function __construct($description = '') {
    $this->description = $description;
  }

  public function complete()
  {
    $this->completed = true;
  }

  public function setDescription($description)
  {
    $this->description = $description;
  }

  public function setCompleted($completed)
  {
    $this->completed = $completed;
  }

  public function getDescription()
  {
    return $this->description;
  }
  
  public function getCompleted()
  {
    return $this->completed;
  }

  public static function all()
  {
    return App::get('database')->selectAll('tasks', 'Task');
  }

  public static function add($task)
  {
    App::get('database')->insert('tasks', $task);
  }
}