<?php
class QueryBuilder
{
  protected $pdo;

  public function __construct(PDO $pdo) {
    $this->pdo = $pdo;
  }
  public function selectAll($table, $class)
  {
    $class = "App\\Models\\{$class}";

    $statement = $this->pdo->prepare("select * from $table");
    $statement->execute();

    return $statement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class);
  }
  public function insert($table, $parameters)
  {
    
    $sql = sprintf(
      "insert into %s (%s) values (%s)",
      $table,
      implode(', ', array_keys($parameters)),
      ':' . implode(', :', array_keys($parameters))
    );

    try {

      $statement = $this->pdo->prepare($sql);
      $statement->execute($parameters);

    } catch (PDOException $e) {

      die('500');

    }
    
  }
}
